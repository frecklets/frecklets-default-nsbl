doc:
  short_help: Ensure the parent folder of a path exists.
  help: |
    Ensure the parent folder of a path exists. If the ``owner`` and/or ``group`` variables is/are specified, those will be created if they don't exist yet
    and set alongside the mode for that folder.

    If the parent folder already exists and owner/group/mode attributes are provided, those won't be applied.

    If a ``owner`` value is provided, this will use 'root' permissions to (potentially) create the parent folder, and the file.

  examples:
    - title: Create parent folder for a path.
      desc: |
        Create folder ``/tmp/parent_1/parent_2``. Doesn't do anything else, like 'touching' the ``freckles.txt`` file.
      vars:
        path: /tmp/parent_1/parent_2/freckles.txt

args:
  path:
    doc:
      short_help: The path to the child file/folder.
    type: string
    required: true
    cli:
      param_type: argument
  owner:
    doc:
      short_help: The owner of the folder, will be created if necessary.
    type: string
    required: false
    cli:
      metavar: USER_NAME
  group:
    doc:
      short_help: The group of the folder, will be created if necessary.
    type: string
    required: false
    cli:
      metavar: GROUP_NAME
  mode:
    doc:
      short_help: The permissions of the folder.
    type: string
    required: false
    cli:
      metavar: MODE
#  become:
#    doc:
#      short_help: Whether to use root privileges to create the folder.
#    type: boolean
#    default: false
#    required: false
#    cli:
#      is_flag: true
  system_user:
    doc:
      short_help: Whether the user and group should be of system user/group type.
    type: boolean
    required: false
    default: false
    cli:
      show_default: true
      is_flag: true
meta:
  is_interface: true
  tags:
    - file
    - filesystem
    - folder
    - mkdir
    - featured-frecklecutable

frecklets:

  - folder-exists:
#      frecklet::desc:
#        long: |
#          Create parent folder for '{{:: path ::}}' (if it doesn't exist yet). Fail if it already exists and is not a folder.
#
#          {%:: if mode ::%}Ensure the folders mode is '{{:: mode ::}}' (recursively, incl. children){%:: else ::%}Leave the folders mode as is (or default if folder has to be created){%:: endif ::%}. {%:: if group ::%}Set group (recursively, incl. children) to be '{{:: group ::}}'{%:: else ::%}Use the executing users main group (or leave be if file already exists){%:: endif ::%} and {%:: if owner ::%}set user to be '{{:: owner ::}}' (recursively){%:: else ::%}use the executing users name as the owner (or leave be if file already exists){%:: endif ::%}.
      path: "{{:: path | dirname ::}}"
      owner: "{{:: owner ::}}"
      group: "{{:: group ::}}"
      mode: "{{:: mode ::}}"
      system: "{{:: system_user ::}}"
      force_chown: false

