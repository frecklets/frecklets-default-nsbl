doc:
  short_help: Makes sure a file/folder has a certain owner/group.
  help: |
    Make sure a file/folder has a certain owner/group.

    This will recursively apply the owner/group change in case the path is a directory.
    If the path does not exist, an empty file will be created.

    Root/sudo permissions will be used to do the chown.

    If the owner/group does not exist on the machine, this will create them before changing the target ownership.
  examples:
    - title: Set group/user/mode attributes on an (existing) file.
      vars:
        path: /tmp/freckles.sh
        owner: freckles
        group: freckles
        mode: "0775"
      dest: |
        If the file ``/tmp/freckles.sh`` exists, this sets its group and owner to 'freckles'. It then sets the mode
        to be '0775' (executable). If the file doesn't exist, nothing will be done.
    
args:
  path:
    doc:
      short_help: the path in question
    type: string
    required: false
  owner:
    doc:
      short_help: the owner of the file/folder
    type: string
    required: false
    cli:
      metavar: USER
  group:
    doc:
      short_help: the group of the file/folder
    type: string
    required: false
    cli:
      metavar: GROUP
  mode:
    doc:
      short_help: The mode to apply.
    type: string
    required: true
  system_user:
    doc:
      short_help: Whether the user and group should be of system user/group type.
    type: boolean
    required: false
    default: false
    cli:
      show_default: true
      is_flag: true
  recursive:
    doc:
      short_help: "Whether to apply the changes recursively (if folder)."
    required: false
    default: false
    type: boolean

meta:
  tags:
    - filesystem
    - file
    - chown
    - featured-frecklecutable

frecklets:
  - group-exists:
      group: "{{:: group ::}}"
      system_group: "{{:: system_user ::}}"
      frecklet::skip: "{{:: group | true_if_empty_or('root') ::}}"
  - user-exists:
      name: "{{:: owner ::}}"
      system_user: "{{:: system_user ::}}"
      frecklet::skip: "{{:: owner | true_if_empty_or('root') ::}}"
  - frecklet:
      name: path-attributes.at.yml
      type: ansible-tasklist
      desc:
        short: "ensure path '{{:: path ::}}' has certain attributes"
      properties:
        elevated: true
        internet: false
        idempotent: true
      resources:
        ansible-tasklist:
          - path-attributes.at.yml
    vars:
      __path__: "{{:: path ::}}"
      __mode__: "{{:: mode ::}}"
      __recurse__: "{{:: recursive ::}}"
      __owner__: "{{:: owner ::}}"
      __group__: "{{:: group ::}}"
