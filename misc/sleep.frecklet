doc:
  short_help: Wait a certain amount of seconds.
  help: |
    In certain situations you need to wait a bit in order for external circumstances to catch up. Ideally you'll
    have a different, more deterministic way of doing so (e.g. the 'wait-for-ssh' frecklet), but in some cases you might have to fall back on a simple 'sleep'.
  examples:
    - title: Wait 10 seconds.
      vars:
        time: 10

args:
  time:
    doc:
      short_help: The amount of seconds to wait.
    type: integer
    required: true

frecklets:
  - frecklet:
      name: wait_for
      type: ansible-module
      desc:
        short: "sleep for {{:: time ::}} seconds"
      properties:
        elevated: false
        idempotent: false
        internet: false
    task:
      connection: local
    vars:
      timeout: "{{:: time ::}}"
