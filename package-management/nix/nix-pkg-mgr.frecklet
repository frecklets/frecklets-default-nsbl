doc:
  short_help: Ensure the 'nix' package manager is installed.
  help: |
    Install the nix package manager for the user running this frecklet.

    This follows the instructions from the [nix homepage](https://nixos.org/nix/download.html).

    NOTE: currently, this does not seem to work

    TODO: allow installation for other users, and multi-user setups.
  furter_reading:
    "nix homepage": https://nixos.org/nix/download.html
  examples:
    - title: Install the 'nix' package manager.
    - title: Install the 'nix' package manager, without verifying the installer signature.
      vars:
        disable_signature_verify: true

args:
  disable_signature_verify:
    required: false
    type: boolean
    doc:
      short_help: don't verify nix installer signature
    default: false
    cli:
      is_flag: true
      param_decls:
        - "--disable-signature-check"

meta:
  status:
    working: false

  tags:
    - nix
    - package-manager
    - package-management
    - install

frecklets:

  - frecklet:
      name: freckfrackery.install-nix
      type: ansible-role
      resources:
        ansible-role:
          - freckfrackery.install-nix
      desc:
        short: installing nix package manager
        references:
          "'freckfrackery.install-nix' Ansible role": "https://gitlab.com/freckfrackery/freckfrackery.install-nix"
        long: |
          Install the [nix](https://nixos.org/nix) package manager. {%:: if disable_signature_verify ::%}Don't verify installer signature.{%:: endif ::%}
      properties:
        idempotent: true
        internet: true
    vars:
       nix_verify_installer: "{{:: disable_signature_verify | negate ::}}"

