doc:
  short_help: Install Hashicorp Vault and run as service.
  help: |
    Create a user named 'vault' (if necessary), download the Vault binary into '/usr/local/bin' and create a
    systemd service unit ('vault') and enable/start it if so specified.

    If 'vault_config' is set, the content of the variable will be stored into '/etc/vault.d/vault.hcl'.

args:
  _import:
    - vault-installed
    - systemd-service-unit
  vault:
    doc:
      short_help: The vault configuration.
    type: dict
    empty: false
    required: false
    keyschema:
      type: string
  vault_config:
    doc:
      short_help: The vault configuration.
    type: dict
    empty: false
    required: false
    keyschema:
      type: string
frecklets:
  - user-exists:
      name: vault
      group: vault
      system_user: true
  - vault-installed:
      version: "{{:: version ::}}"
      dest: "{{:: dest ::}}"
      platform: "{{:: platform ::}}"
      arch: "{{:: arch ::}}"
      owner: "root"
      group: "root"
  - config-values-in-file:
      frecklet::skip: "{{ vault_config | true_if_empty }}"
      path: /etc/vault.d/vault.hcl
      owner: vault
      group: vault
      mode: "0660"
      config: "{{:: vault_config ::}}"
  - systemd-service-unit:
      name: vault
      unit_description: "Hashicorp Vault - a tool for managing secrets"
      unit_documentation:
        - "https://www.vaultproject.io/docs"
      unit_requires:
        - network-online.target
      unit_after:
        - network-online.target
      unit_condition:
        - condition_type: FileNotEmpty
          condition: /etc/vault.d/vault.hcl
      unit_start_limit_interval_sec: 60
      unit_start_limit_burst: 3
      service_user: vault
      service_group: vault
      service_protect_system: full
      service_protect_home: read-only
      service_private_tmp: true
      service_private_devices: true
      service_secure_bits:
        - keep-caps
      service_ambient_capabilities:
        - CAP_IPC_LOCK
      service_capability_bounding_set:
        - CAP_SYSLOG
        - CAP_IPC_LOCK
      service_no_new_privileges: true
      service_exec_start: "/usr/local/bin/vault server -config=/etc/vault.d/vault.hcl"
      service_exec_reload: "/bin/kill --signal HUP $MAINPID"
      service_kill_mode: "process"
      service_kill_signal: "SIGINT"
      service_restart: "on-failure"
      service_restart_sec: 5
      service_timeout_stop_sec: 30
      service_limit:
        - limit_type: NOFILE
          limit: 65536
      install_wanted_by:
        - multi-user.target
      enabled: "{{:: enabled ::}}"
      started: "{{:: started ::}}"



