doc:
  short_help: "Installs PostgreSQL (if necessary), and makes sure a specified database exists."
  help: |
    Ensures a database user with the provided name is present on a PostgreSQL server.

    The PostgreSQL service itself won't be installed, use the 'postgresql-service' frecklet before this if necessary.
  examples:
    - title: Create PostgreSQL user 'freckles' with password 'password123'
      desc: |
        This isn't all that useful on it's own, without the user having access to anything. You probably want the 'postgresql-database-exists'
        frecklet, which incorporates this one.
      vars:
        db_user: freckles
        db_user_password: password123

  references:
    "Postgresql createuser documentation": https://www.postgresql.org/docs/current/app-createuser.html
    "Postgresql 'create role' documentation": https://www.postgresql.org/docs/current/sql-createrole.html
    "Ansible postgresql_user documentation": https://docs.ansible.com/ansible/latest/modules/postgresql_user_module.html
    "Ansible postgresql_db documentation": https://docs.ansible.com/ansible/latest/modules/postgresql_db_module.html

args:
  db_user:
    doc:
      short_help: "The name of the database user ('role' in postgres)."
    type: string
    required: true
  db_user_password:
    doc:
      short_help: "The (hashed) password for the database user ('role' in PostgreSQL)."
      help: |
        The password needs to be passed in hashed form, please check the [Postgresql documentation](
    type: string
    required: false
    secret: true

meta:
  tags:
    - postgresql
    - database

frecklets:
  - task:
      become: true
      become_user: postgres
    frecklet:
      name: postgresql_user
      type: ansible-module
      skip: "{{:: db_user | true_if_empty ::}}"
      properties:
        elevated: true
        idempotent: true
        internet: false
      desc:
        short: "create postgresql user '{{:: db_user ::}}'"
        references:
          "'postgresql_user' Ansible module": "https://docs.ansible.com/ansible/latest/modules/postgresql_user_module.html"
    vars:
      name: "{{:: db_user ::}}"
      password: "{{:: db_user_password | postgresql_password_hash(db_user) ::}}"
      encrypted: true
