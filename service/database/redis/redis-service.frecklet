doc:
  short_help: Install a Redis service.

args:
  port:
    doc:
      short_help: The port to listen on.
    type: integer
    required: false
    default: 6379
  bind:
    doc:
      short_help: The address to listen on (set to '0.0.0.0' to listen on all interfaces).
    type: string
    required: false
    default: 127.0.0.1
  unix_socket:
    doc:
      short_help: If set, Redis will also listen on a local Unix socket.
    type: string
    required: false
  timeout:
    doc:
      short_help: Close a connection after a client is idle N seconds. Set to 0 to disable timeout.
    type: integer
    required: false
  loglevel:
    doc:
      short_help: "Log level (default: notice)."
    type: string
    required: false
    allowed:
      - debug
      - verbose
      - notice
      - warning
  logfile:
    doc:
      short_help: "The log file path."
    type: string
    required: false
  databases:
    doc:
      short_help: "The number of Redis databases (default: 16)."
    type: integer
    required: false
  save:
    doc:
      short_help: "Snapshotting configuration; setting values in this list will save the database to disk if the given number of seconds (e.g. 900) and the given number of write operations (e.g. 1) have occurred. Example for list item: '300 10'."
    type: list
    schema:
      type: string
    required: false
    empty: false
  dbdir:
    doc:
      short_help: "Database path."
    type: string
    required: false
  dbfilename:
    doc:
      short_help: "Filename for db file."
    type: string
    required: false
  db_compression:
    doc:
      short_help: Compress string objects in database.
    type: boolean
    required: false
    default: true
  maxmemory:
    doc:
      short_help: Limit memory usage to the specified amount of bytes. Leave at 0 for unlimited.
    type: integer
    required: false
    default: 0


frecklets:
  - frecklet:
      name: geerlingguy.redis
      type: ansible-role
      properties:
        internet: true
        elevated: true
        idempotent: true
      desc:
        short: install Redis service from system packages
    task:
      become: true
      include-type: import
    vars:
      redis_port: "{{:: port ::}}"
      redis_bind_interface: "{{:: bind ::}}"
      redis_unixsocket: "{{:: unix_socket ::}}"
      redis_timeout: "{{:: timeout ::}}"
      redis_loglevel: "{{:: loglevel ::}}"
      redis_logfile: "{{:: logfile ::}}"
      redis_databases: "{{:: databases ::}}"
      redis_save: "{{:: save ::}}"
      redis_dbdir: "{{:: dbdir ::}}"
      redis_dbcompression: "{%:: if db_compression ::%}yes{%:: else ::%}no{%:: endif ::%}"
      redis_maxmemory: "{{:: maxmemory ::}}"


