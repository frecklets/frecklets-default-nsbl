doc:
  short_help: Download the Syncthing binary.
  help: |
    Download the 'syncthing' binary for the selected architecture/platform and install it.
  examples:
    - title: Install Syncthing for (current) user (into ~/.local/bin).
      vars: {}
    - title: Install Syncthing systemwide (into /usr/local/bin).
      vars:
        owner: root
        group: root
    - title: Install Syncthing systemwide to custom location.
      vars:
        owner: root
        group: root
        dest: /opt/syncthing

args:
  _import: executable-downloaded
  version:
    doc:
      short_help: The version of the product.
    type: string
    required: true
    default: 1.2.1
  arch:
    doc:
      short_help: The architecture of the host system.
    type: string
    required: false
    default: amd64
    allowed:
      - amd64
      - "386"
      - arm
      - arm64
      - mips
      - mips64
      - mipsle
      - ppc64le
      - s390x
  platform:
    doc:
      short_help: The platform of the host system.
    type: string
    required: false
    default: linux
    allowed:
      - linux
      - freebsd
      - darwin
      - openbsd
      - solaris
      - windows
  owner:
    doc:
      short_help: "The user who runs syncthing."
    type: string
    required: false
    cli:
      metavar: USER
  group:
    doc:
      short_help: "The group for the syncthing executable."
    type: string
    required: false
    cli:
      metavar: GROUP


frecklets:
  - executable-downloaded:
      url: "https://github.com/syncthing/syncthing/releases/download/v{{:: version ::}}/syncthing-{{:: platform ::}}-{{:: arch ::}}-v{{:: version ::}}.tar.gz"
      dest: "{{:: dest ::}}"
      exe_mode: "0775"
      exe_name: "syncthing"
      extract_subfolder: "syncthing-{{:: platform ::}}-{{:: arch ::}}-v{{:: version ::}}"
      extract: true
      group: "{{:: group ::}}"
      owner: "{{:: owner ::}}"
      force: "{{:: force ::}}"

