doc:
  short_help: "Ensures the nginx web server is installed and running."
  help: |
    Installs the Nginx web server.

    This uses the [geerlingguy.nginx](https://github.com/geerlingguy/ansible-role-nginx)
    Ansible role to do the heavy lifting.

    It's recommended to use the 'webserver-service' frecklet instead of this if you want to provision a web server
    with things like https certificate, php installed, etc.
  furter_reading:
    "nginx website": https://www.nginx.com
    "geerlingguy.nginx Ansible role": https://github.com/geerlingguy/ansible-role-nginx
  examples:
    - title: Install Nginx webserver
    - title: Install Nginx webserver, use *freckles* user as the user who runs the Nginx service.
      desc: |
        Install Nginx webserver, use *freckles* user as the user who runs the Nginx service.
        The *freckles* user has to exists already for this to work.

        This is useful for example when you develop using Vagrant or a similar tool, and can't easily change the owner/permissions
        of files Nginx is supposed to server. Much easier to just change the user who runs Nginx than trying to muck
        about with mount options and such.
      vars:
        user: freckles
    - title: Install Nginx webserver, keep default vhost config in place.
      vars:
        remove_default_vhost: false

args:
  user:
    type: string
    required: false
    doc:
      short_help: the user nginx will run under
  remove_default_vhost:
    type: boolean
    doc:
      short_help: Whether to remove the 'default' virtualhost configuration supplied by Nginx.
      help: |
        Whether to remove the 'default' virtualhost configuration supplied by Nginx. Useful if you want the base / URL to be directed at one of your own virtual hosts configured in a separate .conf file.
    required: false
    default: true
    cli:
      param_decls:
        - "--remove-default-vhost"

meta:
  tags:
    - webserver
    - nginx
    - featured-frecklecutable

frecklets:
  - task:
      include-type: import
      become: true
    frecklet:
      type: ansible-role
      resources:
        ansible-role:
          - geerlingguy.nginx
      name: geerlingguy.nginx
      properties:
        idempotent: true
        elevated: true
        internet: true
      desc:
        short: "install nginx web server"
        references:
          "'geerlingguy.nginx' Ansible role": "https://github.com/geerlingguy/ansible-role-nginx"
    vars:
      nginx_user: "{{:: user ::}}"
      nginx_remove_default_vhost: "{{:: remove_default_vhost ::}}"
