doc:
  short_help: Make sure a list of users exist on a system.
  help: |
    Make sure a list of users exist on a system.

    This is a fairly basic frecklet, use the 'user-exist' one if you need to specify details about users.

  references:
    "Creating a User in Ansible": https://serversforhackers.com/c/create-user-in-ansible

args:
  names:
    doc:
      short_help: A list of usernames to make sure exist.
    type: list
    required: true
    empty: false
    schema:
      type: string
    cli:
      metavar: USER_NAME
      param_type: argument
  system_user:
    doc:
      short_help: Whether the users to create should be created as system user.
    type: boolean
    required: false
    default: false
    cli:
      show_default: true
      is_flag: true

meta:
  tags:
    - user
    - user-management
    - system

frecklets:
  - frecklet:
      name: user
      type: ansible-module
      desc:
        short: "ensure users exit: {{:: names | join(', ') ::}}"
        references:
          "'useradd' tutorial (tecmint)": "https://www.tecmint.com/add-users-in-linux/"
          "'user' Ansible module": "https://docs.ansible.com/ansible/latest/modules/user_module.html"
      properties:
        idempotent: true
        elevated: true
        internet: false
    task:
      become: true
      loop: "{{:: names ::}}"
      loop_control:
        loop_var: "__name__"
    vars:
      name: "{{ __name__ }}"
      state: present
      system: "{{:: system_user ::}}"
