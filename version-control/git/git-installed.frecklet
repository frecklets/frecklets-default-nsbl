doc:
  short_help: Ensures git is installed.
  help: |
    Ensures git is installed if not available.

    Currently this only uses the system package manager, which means root permissions are required. In the future also
    (at least) a conda install will be supported.

frecklets:
- frecklet:
    name: install-git.at.yml
    type: ansible-tasklist
    desc:
      short: "install git (if necessary)"
      long: |
        Check if the 'git' executable is available and working (Mac OS X installs only a wrapper script that does not work by default).

        If not available, install either the 'git' system package (on Linux), or the [Mac OS X command-line tools)(https://freckles.io/frecklets/default/platform-specific/osx-command-line-tools-installed) (which include 'git').

    properties:
      idempotent: true
      internet: true
      elevated: true
    resources:
      ansible-role:
        - geerlingguy.homebrew
        - elliotweiser.osx-command-line-tools
  task:
    become: true
  vars:
    name:
      - git
