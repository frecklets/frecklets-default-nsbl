doc:
  short_help: Add a deploy key to a GitLab project.
  help: |
    Add a deploy key to a GitLab project.

    This can be used to authorize your (or a host-) ssh public key to be able to pull from private repositories, or push a public or private repository. Useful for CI pipelines and such.

args:
  _import: ssh-key-exists
  key_content:
    doc:
      short_help: The content of the public ssh key.
    type: string
    required: true
    excludes: path
  path_to_key:
    doc:
      short_help: The path to the private ssh key.
      help: |
        The path to the private ssh key.

        The path to the public key will be infered (added '.pub').
    type: string
    required: true
    excludes: key_content
  access_token:
    doc:
      short_help: The Gitlab access token.
    type: string
    required: true
    secret: true
  gitlab_user:
    doc:
      short_help: The Gitlab user- or organization name.
    type: string
    required: true
  gitlab_project:
    doc:
      short_help: The name of the Gitlab project.
    type: string
    required: true
  deploy_key_title:
    doc:
      short_help: The title of the deploy key on Gitlab.
    type: string
    required: false
    default: deploy
  can_push:
    doc:
      short_help: Whether the key in question is allowed to push to the repo.
    type: boolean
    default: false
    cli:
      param_decls:
        - "--can-push"

frecklets:
  - ssh-key-exists:
#      frecklet::skip: "{{:: path_to_key | true_if_empty ::}}"
      path: "{{:: path_to_key ::}}"
      password: "{{:: password ::}}"
      user: "{{:: user ::}}"
      key_type: rsa
      bits: "{{:: bits ::}}"
  - frecklet:
      name: add-gitlab-deploy-key.at.yml
      type: ansible-tasklist
      properties:
        elevated: false
        internet: true
        idempotent: true
      desc:
        short: "add deploy key with alias '{{:: deploy_key_title ::}}' to GitLab project '{{:: gitlab_user ::}}/{{:: gitlab_project ::}}'"
        long: |
          {%:: if path -::%}
          Read the public key file '{{:: path_to_key ::}}.pub' and use the the content to create or overwrite a trusted deploy key for the specified GitLab project, using the following details:{%:: else -::%}
          Use the 'key_content' string ( {{:: key_content ::}} ) to create or overwrite a trusted deploy key for the specifieed GitLab project, using the following details:{%:: endif ::%}

              gitlab access token: <your secret token>
              gitlab user/organization: {{:: gitlab_user ::}}
              gitlab project: {{:: gitlab_project ::}}
              deploy key alias: {{:: deploy_key_title ::}}
              'push' allowed for key: {{:: can_push ::}}
        references:
          "GitLab deploy key documentation": "https://docs.gitlab.com/ee/ssh/"
      resources:
        python-package:
          - python-gitlab
        ansible-tasklist:
          - add-gitlab-deploy-key.at.yml
          - internally-register-public-ssh-key.at.yml
    task:
      become: false
    vars:
      __path_to_key__: "{{:: path_to_key ::}}"
      __key_content__: "{{:: key_content ::}}"
      __project_name__: "{{:: gitlab_user ::}}/{{:: gitlab_project ::}}"
      __gitlab_private_access_token__: "{{:: access_token ::}}"
      __deploy_key_title__: "{{:: deploy_key_title ::}}"
      __can_push__: "{{:: can_push ::}}"
      __use_become__: "{{:: user | false_if_empty ::}}"

