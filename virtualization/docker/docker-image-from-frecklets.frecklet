doc:
  short_help: Build a Docker image from a frecklet.
  help: |
    Build a Docker image from a frecklet.

    This is just a proof-of-concept, to show that sometime like this is easily possible. It currently only works with Debian-based base images, as the pre-written Dockerfile template uses 'apt' to install curl. This would be easily changed once this is done properly.
  references:
    "'docker_image' Ansible module": "https://docs.ansible.com/ansible/latest/modules/docker_image_module.html"
  examples:
    - title: Build a docker image with Java installed.
      vars:
        frecklets:
          - java-lang-installed
        name: freckles-java
    - title: Build a docker image with both Java & Go installed
      desc: |
        Don't use the Docker cache, and build the image even if an image named 'freckles-java-go' already exists.
      vars:
        name: freckles-java-go
        frecklets:
          - java-lang-installed
          - go-lang-installed
        force: true
        no_cache: true

args:
  _import: docker-image-from-folder
  frecklets:
    doc:
      short_help: The frecklets to use in the Docker container.
    type: list
    schema:
      type: string
    required: true
    empty: false
    cli:
      param_decls:
        - "--frecklet"
        - "-f"
  base_image:
    doc:
      short_help: The base image to use.
    type: string
    default: ubuntu:18.04
    required: false
frecklets:
  - path-is-absent:
      path: "/tmp/_frecklet_docker_build/{{:: name ::}}"
  - folder-exists:
      path: "/tmp/_frecklet_docker_build/{{:: name ::}}"
      mode: "0700"
  - file-with-content:
      path: "/tmp/_frecklet_docker_build/{{:: name ::}}/Dockerfile"
      content: |
        FROM {{:: base_image ::}}

        # necessary if no locale is set
        ENV LC_CTYPE C.UTF-8

        RUN apt update && apt install -y curl
        RUN curl https://freckles.sh | bash
        {%:: for frecklet in frecklets ::%}
        RUN /root/.local/share/freckles/bin/frecklecute --community {{:: frecklet ::}}
        {%:: endfor ::%}
        RUN rm -rf /root/.local/share/freckles
        RUN rm -rf /root/.cache/freckles
  - docker-image-from-folder:
      name: "{{:: name ::}}"
      base_path: "/tmp/_frecklet_docker_build/{{:: name ::}}"
      nocache: "{{:: nocache ::}}"
      force: "{{:: force ::}}"
