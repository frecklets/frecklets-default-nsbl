doc:
  short_help: "Makes sure Docker is installed."
  help: |
    Installs Docker on a Linux machine.

    If the ``users`` variable is set, those users will be created if they don't exist yet. Do this seperately if you need to have more
    control about user creation (e.g. to provide password, ssh-pub-keys, etc.)

    Windows and Mac OS X are not supported just yet.

  references:
    "docker homepage": https://docker.com
    "geerlingguy.docker Ansible role": https://github.com/geerlingguy/ansible-role-docker
  examples:
    - title: Install docker, adding the 'freckles' user to the 'docker' group.
      vars:
        users:
          - freckles

args:
  users:
    doc:
      short_help: "A list of users who will be added to the 'docker' group."
    required: false
    type: list
    cli:
      param_decls:
        - "--user"
        - "-u"
      metavar: USER
  enabled:
    doc:
      short_help: "Whether to enable the docker service or not."
    type: boolean
    required: false
    default: true
    cli:
      is_flag: true
  started:
    doc:
      short_help: "Whether to start the docker service or not."
    type: boolean
    required: false
    default: true
    cli:
      is_flag: true

meta:
  tags:
    - install
    - docker
    - container
    - featured-frecklecutable
    - virtualization

frecklets:
  - git-installed
  - pip-requirements-present
  - task:
      become: true
    frecklet:
      name: pip
      type: ansible-module
      desc:
        short: install docker client pip package
        references:
          "'pip' Ansible module": "https://docs.ansible.com/ansible/latest/modules/pip_module.html"
      properties:
        idempotent: true
        elevated: true
        internet: true
    vars:
      name: docker
  - task:
      become: true
      include-type: import
    frecklet:
      name: geerlingguy.docker
      type: ansible-role
      resources:
        ansible-role:
          - geerlingguy.docker
      desc:
        short: install docker service
        references:
          "'geerlingguy.docker' Ansible role": "https://github.com/geerlingguy/ansible-role-docker"
      properties:
        idempotent: true
        internet: true
        elevated: true
    vars:
      docker_users: "{{:: users ::}}"
      docker_service_state: "{{:: started | string_for_boolean('started', 'stopped') ::}}"
      docker_service_enabled: "{{:: enabled ::}}"
