doc:
  short_help: "Install the curl package."
  help: |
    Install the '[curl](https://curl.haxx.se/) utility.
  examples:
    - title: Install the 'curl' package using the system package manager.
    - title: Install the 'curl' package using the 'nix' package manager.
      vars:
        pkg_mgr: nix

args:
  pkg_mgr:
    doc:
      short_help: the package manager to use
    type: string
    default: auto
    required: false
    cli:
      metavar: PKG-MGR

meta:
  tags:
    - sshpass
    - install
    - pkg

frecklets:
  - packages-installed:
      become: "{{:: pkg_mgr | false_if_equal('conda') ::}}"
      packages:
        - name: curl
          pkg_mgr: "{{:: pkg_mgr ::}}"
